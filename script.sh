#!/bin/bash
 
set -uxoe pipefail

INPUT=data.csv
OUTPUT=output_file.csv

# Массив
names=(Tom Robert Leonardo Brad Matt Vin Keanu Will Chris George)
surnames=(Cruise Downey DiCaprio Pitt Damon Diese Reeves Smith Evans Clooney)

random_name(){
	index=$((RANDOM % ${#names[@]}))   
	echo ${names[$index]} 
}

random_surname(){
	index=$((RANDOM % ${#surnames[@]}))   
	echo ${surnames[$index]} 
}

random_phone() {
	echo "93"$(cat /dev/random | tr -dc '0-9' | head -c 9)
}

header=$(head -n 1 $INPUT)
echo $header > $OUTPUT

while IFS=, read -r f_name l_name email phone address
do
	# printf "$f_name "
	# printf "$l_name "
	# printf "$phone \n"
	new_name=$(random_name)
	new_surname=$(random_surname)
	new_phone=$(random_phone)

	line="$new_name,$new_surname,$email,$new_phone,$address"
	echo "$line" >> $OUTPUT

done < $INPUT 

PID=$$
echo $PID

sleep $((RANDOM % 30))
kill -9 $PID


